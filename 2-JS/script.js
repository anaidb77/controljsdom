
"use strict";

// puntuaciones
const puntuaciones = [
  {
    equipo: "Toros Negros",
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: "Amanecer Dorado",
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: "Águilas Plateadas",
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: "Leones Carmesí",
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: "Rosas Azules",
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: "Mantis Verdes",
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: "Ciervos Celestes",
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: "Pavos Reales Coral",
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: "Orcas Moradas",
    puntos: [2, 3, 3, 4],
  },
];




let bisPuntuaciones = JSON.parse(JSON.stringify(puntuaciones));
bisPuntuaciones = bisPuntuaciones

      .map((equipo) => {
        equipo.totales = equipo.puntos.reduce(
          (acumulador, puntos) => (acumulador = acumulador + puntos)
        );
        return equipo;
         })

      .sort((first, last) => {
    
       if (first.totales >= last.totales) {
          return +1;
        } else {
        return -1;
        }
      }); 
    console.log(
      `El equipo que obtuvo menos puntuación es ${bisPuntuaciones[0].equipo} con un total de
       ${bisPuntuaciones[0].totales} puntos.`);

     console.log(
        `El equipo con más puntuación será ${
          bisPuntuaciones[bisPuntuaciones.length - 1].equipo} con una puntuacion de ${
          bisPuntuaciones[bisPuntuaciones.length - 1].totales} puntos totales`
      );