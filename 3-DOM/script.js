"use strict";

const body = document.querySelector("body");
const div = document.createElement("div");
const h1 = document.createElement("h1");

div.appendChild(h1);
body.appendChild(div);

setInterval(() => {
  let fecha = new Date();
  let hora = fecha.getHours();
  let minuto = fecha.getMinutes();
  let segundo = fecha.getSeconds();
  if (hora < 10) {
    hora = "0" + hora;
  }
  if (minuto < 10) {
    minuto = "0" + minuto;
  }
  if (segundo < 10) {
    segundo = "0" + segundo;
  }
  let verReloj = hora + " : " + minuto + " : " + segundo;
  h1.textContent = `${hora}:${minuto}:${segundo}`;
}, 1000);
